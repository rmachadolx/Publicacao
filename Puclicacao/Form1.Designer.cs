﻿namespace Publicacao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TextBoxPublicacao = new System.Windows.Forms.TextBox();
            this.LabelPublicacao = new System.Windows.Forms.Label();
            this.ButtonPublicar = new System.Windows.Forms.Button();
            this.ButtonNaoGostar = new System.Windows.Forms.Button();
            this.ButtonGostar = new System.Windows.Forms.Button();
            this.LabelGosto = new System.Windows.Forms.Label();
            this.LabelNaoGosto = new System.Windows.Forms.Label();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBoxPublicacao
            // 
            this.TextBoxPublicacao.Location = new System.Drawing.Point(91, 52);
            this.TextBoxPublicacao.Multiline = true;
            this.TextBoxPublicacao.Name = "TextBoxPublicacao";
            this.TextBoxPublicacao.Size = new System.Drawing.Size(319, 108);
            this.TextBoxPublicacao.TabIndex = 0;
            // 
            // LabelPublicacao
            // 
            this.LabelPublicacao.AutoSize = true;
            this.LabelPublicacao.Location = new System.Drawing.Point(88, 227);
            this.LabelPublicacao.Name = "LabelPublicacao";
            this.LabelPublicacao.Size = new System.Drawing.Size(0, 13);
            this.LabelPublicacao.TabIndex = 1;
            // 
            // ButtonPublicar
            // 
            this.ButtonPublicar.Location = new System.Drawing.Point(334, 196);
            this.ButtonPublicar.Name = "ButtonPublicar";
            this.ButtonPublicar.Size = new System.Drawing.Size(75, 23);
            this.ButtonPublicar.TabIndex = 2;
            this.ButtonPublicar.Text = "Publicar";
            this.ButtonPublicar.UseVisualStyleBackColor = true;
            this.ButtonPublicar.Click += new System.EventHandler(this.ButtonPublicar_Click);
            // 
            // ButtonNaoGostar
            // 
            this.ButtonNaoGostar.Image = global::Puclicacao.Properties.Resources.deslike;
            this.ButtonNaoGostar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonNaoGostar.Location = new System.Drawing.Point(290, 321);
            this.ButtonNaoGostar.Name = "ButtonNaoGostar";
            this.ButtonNaoGostar.Size = new System.Drawing.Size(120, 50);
            this.ButtonNaoGostar.TabIndex = 4;
            this.ButtonNaoGostar.Text = "Não Gosto";
            this.ButtonNaoGostar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonNaoGostar.UseVisualStyleBackColor = true;
            this.ButtonNaoGostar.Click += new System.EventHandler(this.ButtonNaoGostar_Click);
            // 
            // ButtonGostar
            // 
            this.ButtonGostar.Image = ((System.Drawing.Image)(resources.GetObject("ButtonGostar.Image")));
            this.ButtonGostar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonGostar.Location = new System.Drawing.Point(91, 321);
            this.ButtonGostar.Name = "ButtonGostar";
            this.ButtonGostar.Size = new System.Drawing.Size(120, 50);
            this.ButtonGostar.TabIndex = 3;
            this.ButtonGostar.Text = "Gosto";
            this.ButtonGostar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonGostar.UseVisualStyleBackColor = true;
            this.ButtonGostar.Click += new System.EventHandler(this.ButtonGostar_Click);
            // 
            // LabelGosto
            // 
            this.LabelGosto.AutoSize = true;
            this.LabelGosto.Location = new System.Drawing.Point(217, 340);
            this.LabelGosto.Name = "LabelGosto";
            this.LabelGosto.Size = new System.Drawing.Size(0, 13);
            this.LabelGosto.TabIndex = 5;
            // 
            // LabelNaoGosto
            // 
            this.LabelNaoGosto.AutoSize = true;
            this.LabelNaoGosto.Location = new System.Drawing.Point(416, 340);
            this.LabelNaoGosto.Name = "LabelNaoGosto";
            this.LabelNaoGosto.Size = new System.Drawing.Size(0, 13);
            this.LabelNaoGosto.TabIndex = 6;
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(211, 379);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(0, 13);
            this.LabelStatus.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 401);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.LabelNaoGosto);
            this.Controls.Add(this.LabelGosto);
            this.Controls.Add(this.ButtonNaoGostar);
            this.Controls.Add(this.ButtonGostar);
            this.Controls.Add(this.ButtonPublicar);
            this.Controls.Add(this.LabelPublicacao);
            this.Controls.Add(this.TextBoxPublicacao);
            this.Name = "Form1";
            this.Text = "Posts";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxPublicacao;
        private System.Windows.Forms.Label LabelPublicacao;
        private System.Windows.Forms.Button ButtonPublicar;
        private System.Windows.Forms.Button ButtonGostar;
        private System.Windows.Forms.Button ButtonNaoGostar;
        private System.Windows.Forms.Label LabelGosto;
        private System.Windows.Forms.Label LabelNaoGosto;
        private System.Windows.Forms.Label LabelStatus;
    }
}

