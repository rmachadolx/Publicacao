﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Publicacao
{
   
    public partial class Form1 : Form
    {
        Post post = new Post();
        public Form1()
        {
            InitializeComponent();
            ButtonGostar.Enabled = false;
            ButtonNaoGostar.Enabled = false;
        }

        private void ButtonPublicar_Click(object sender, EventArgs e)
        {
            post.Publicacao = TextBoxPublicacao.Text;
            post.Data = DateTime.Now;
            LabelPublicacao.Text = post.Publicacao;
            ButtonGostar.Enabled = true;
            ButtonNaoGostar.Enabled = true;
        }

        private void ButtonGostar_Click(object sender, EventArgs e)
        {
            post.Gostar();
            LabelGosto.Text = post.Like.ToString();
            LabelStatus.ForeColor = Color.Green;
            LabelStatus.Text = post.Mensagem;

        }

        private void ButtonNaoGostar_Click(object sender, EventArgs e)
        {
            post.NaoGostar();
            LabelNaoGosto.Text = post.Deslike.ToString();
            LabelStatus.ForeColor = Color.Red;
            LabelStatus.Text = post.Mensagem;
        }

    }
}
