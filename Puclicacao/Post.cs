﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Publicacao
{
    using System;
    public class Post
    {
        #region Atributos
        private string _publicacao;
        private DateTime _data;
        private int _like;
        private int _deslike;
        private string _mensagem;
        #endregion

        #region Propriedades
        public string Publicacao
        {
            get { return _publicacao; }
            set { _publicacao = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }
        public int Like
        {
            get { return _like; }
            set { _like = value; }
        }
        public int Deslike
        {
            get { return _deslike; }
            set { _deslike = value; }
        }
        public string Mensagem
        {
            get { return _mensagem; }

            set { _mensagem = value; }
        }
        #endregion

        #region Metodos

        public Post() : this(" ", DateTime.Now, 0, 0, "") { }

        public Post(string _publicacao, DateTime _data, int _like, int _deslike, string _mensagem)
        {
            this.Publicacao = _publicacao;
            this.Data = _data;
            this.Like = _like;
            this.Deslike = _deslike;
            this.Mensagem = _mensagem;


        }
        public int Gostar()
        {
            Like++;
            _mensagem = "Colocou Gosto";
            return Like;
        }
        public int NaoGostar()
        {
            Deslike++;
            _mensagem = "Colocou Não Gosto!";
            return Deslike;
        }
        #endregion
    }
}
